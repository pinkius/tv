/*
 * Copyright 2013 Webster Smalley
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.webstersmalley.tv.sky;

import com.webstersmalley.tv.db.TvDao;
import com.webstersmalley.tv.domain.Recording;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by: Matthew Smalley
 * Date: 14/04/13
 */
@Service("skyService")
public class SkyService {
    @Resource(name = "skyBrowserService")
    private SkyBrowserService skyBrowserService;

    @Resource(name = "tvDao")
    private TvDao tvDao;

    public void importRecordings() {
        tvDao.deleteAllRecordings();
        for (Recording recording : skyBrowserService.getListOfRecordings()) {
            tvDao.addRecording(recording);
        }
    }

    public static void main(String[] args) {
        if (System.getProperty("environment") == null) {
            System.setProperty("environment", "dev");
        }
        if (System.getProperty("target") == null) {
            System.setProperty("target", "file");
        }
        ApplicationContext ac = new ClassPathXmlApplicationContext("environment.xml");
        SkyService commandLineSearcher = ac.getBean("skyService", SkyService.class);
        commandLineSearcher.importRecordings();
    }
}
