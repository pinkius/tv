/*
 * Copyright 2013 Webster Smalley
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.webstersmalley.tv.comms;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.io.InputStream;
import java.net.URL;

@Repository("classloaderComms")
public class ClassloaderComms implements Comms {
    private final Logger logger = LoggerFactory.getLogger(getClass());

    public String getURLAsString(String resource) {
        logger.debug("serving: {}", resource);
        try {
            String[] bits = resource.split("/");
            String filename = "dev/" + bits[bits.length - 1];
            logger.debug("Getting {} from classpath", filename);
            InputStream is = this.getClass().getClassLoader()
                    .getResourceAsStream(filename);
            String contents = IOUtils.toString(is);
            IOUtils.closeQuietly(is);
            return contents;
        } catch (Exception e) {
            return null;
            //throw new RuntimeException("Error getting url", e);
        }

    }

    @Override
    public String getResourceByRawHttp(URL url, String request) {
        logger.debug("sending {} to {}", request, url);
        String resourceName = DigestUtils.md5Hex(request);
        return getURLAsString(resourceName);
    }
}
