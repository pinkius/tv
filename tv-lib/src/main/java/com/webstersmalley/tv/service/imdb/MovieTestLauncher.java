package com.webstersmalley.tv.service.imdb;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.webstersmalley.tv.db.TableDumper;
import com.webstersmalley.tv.db.TvDao;
import com.webstersmalley.tv.domain.Program;
import com.webstersmalley.tv.service.ListingsRefresher;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.IOException;
import java.math.BigDecimal;

/**
 * Created by: Matthew Smalley
 * Date: 13/05/13
 */
@Service("movieTestLauncher")
public class MovieTestLauncher {

    @Resource(name = "listingsRefresher")
    private ListingsRefresher listingsRefresher;

    @Resource(name = "tableDumper")
    private TableDumper tableDumper;

    @Resource(name = "imdbDataRetriever")
    private ImdbDataRetriever imdbDataRetriever;

    public void go () {
        listingsRefresher.refreshListings();
        System.out.println("dumping");
        tableDumper.dumpTable("select * from vprograms where movie = true");
        //tableDumper.dumpTable("select channels.name, vprograms.year, vprograms.starttime, vprograms.title, moviemetadata.rating, moviemetadata.imdb_id from movieMetadata, vprograms, channels where channels.id = vprograms.channelid and vprograms.id = moviemetadata.programid");
        tableDumper.dumpTable("select count(*) from vprograms where movie = true");
        tableDumper.dumpTable("select count(*) from vprograms where movie = true and imdbId is not null");
        tableDumper.dumpTable("select count(*) from vprograms where movie = true and imdbRating is not null");
        System.out.println("done");
    }

    public static void main(String[] args) {
        System.setProperty("environment", "longtimeout");
        System.setProperty("target", "file");
        ApplicationContext ac = new ClassPathXmlApplicationContext("environment.xml");
        MovieTestLauncher movieTestLauncher = ac.getBean("movieTestLauncher", MovieTestLauncher.class);
        movieTestLauncher.go();
    }
}
