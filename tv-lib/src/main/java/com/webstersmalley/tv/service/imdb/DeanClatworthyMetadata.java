package com.webstersmalley.tv.service.imdb;

import org.apache.commons.lang.builder.ToStringBuilder;

import java.math.BigDecimal;

/**
 * Created by: Matthew Smalley
 * Date: 18/05/13
 */
public class DeanClatworthyMetadata {

    String imdbid;
    String rating;

    public String getImdbid() {
        return imdbid;
    }

    public void setImdbid(String imdbid) {
        this.imdbid = imdbid;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}