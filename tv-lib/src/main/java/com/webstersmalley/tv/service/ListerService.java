/*
 * Copyright 2013 Webster Smalley
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.webstersmalley.tv.service;

import com.webstersmalley.tv.domain.Channel;
import com.webstersmalley.tv.domain.Program;
import com.webstersmalley.tv.domain.SearchQuery;

import java.util.List;

/**
 * Created by: Matthew Smalley
 * Date: 09/04/12
 */
public interface ListerService {

    Channel getChannel(String channelName);

    Channel getChannelById(int channelId);

    List<Program> getProgramsByTitle(String programTitle);

    List<Program> getProgramsBySearchQuery(SearchQuery searchQuery);

    List<Program> getProgramsBySimpleSearchQuery(String query);

    List<Program> getProgramsByTitleAndChannel(
            String programTitle, String channel);

    List<Channel> getChannels();

    List<Program> getProgramsByChannel(Channel channel);

}
