/*
 * Copyright 2013 Webster Smalley
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.webstersmalley.tv.service;

import com.webstersmalley.tv.db.SearchQueryDao;
import com.webstersmalley.tv.domain.SearchQuery;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by: Matthew Smalley
 * Date: 29/09/12
 */
@Service("searchQueryService")
public class SearchQueryServiceImpl implements SearchQueryService {
    @Resource(name = "searchQueryDao")
    private SearchQueryDao searchQueryDao;

    public void setSearchQueryDao(SearchQueryDao searchQueryDao) {
        this.searchQueryDao = searchQueryDao;
    }

    @Override
    public List<SearchQuery> getSearchQueries() {
        return searchQueryDao.getSearchQueries();
    }

    @Override
    public void saveSearchQuery(SearchQuery searchQuery) {
        searchQueryDao.saveSearchQuery(searchQuery);
    }
}
