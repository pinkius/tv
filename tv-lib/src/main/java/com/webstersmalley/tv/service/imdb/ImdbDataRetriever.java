package com.webstersmalley.tv.service.imdb;

import com.webstersmalley.tv.domain.Program;

/**
 * Created by: Matthew Smalley
 * Date: 13/05/13
 */
public interface ImdbDataRetriever {
    void addImdbData(Program program);
}
