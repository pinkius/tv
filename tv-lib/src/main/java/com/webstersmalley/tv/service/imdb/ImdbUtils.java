package com.webstersmalley.tv.service.imdb;

import java.math.BigDecimal;

/**
 * Created by: Matthew Smalley
 * Date: 18/05/13
 */
public final class ImdbUtils {
    private ImdbUtils() {

    }

    public final static BigDecimal convertString(String input) {
        try {
            return new BigDecimal(input);
        } catch (Exception e) {
            return null;
        }
    }

    public final static String encode(String input) {
        return input.replaceAll(" ", "%20").replaceAll("\"", "").replaceAll("&", "%26").replaceAll("!", "");
    }
}
