package com.webstersmalley.tv.service.imdb;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.webstersmalley.tv.comms.Comms;
import com.webstersmalley.tv.domain.Program;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.IOException;

import static com.webstersmalley.tv.service.imdb.ImdbUtils.convertString;
import static com.webstersmalley.tv.service.imdb.ImdbUtils.encode;
/**
 * Created by: Matthew Smalley
 * Date: 13/05/13
 */
@Service("deanClatworthyImdbDataRetriever")
public class DeanClatworthyMetadataRetriever implements ImdbDataRetriever {
    private Logger logger = LoggerFactory.getLogger(getClass());

    @Resource(name = "longtimeoutComms")
    private Comms comms;

    private ObjectMapper mapper = new ObjectMapper();

    public DeanClatworthyMetadataRetriever() {
        mapper.configure(JsonGenerator.Feature.QUOTE_FIELD_NAMES, false);
        mapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
    }
    private final static String SOURCE="DC";



    @Override
    public void addImdbData(Program program) {
        try {
            String query = "http://deanclatworthy.com/imdb/?q=" + encode(program.getTitle());
            if (program.getYear() != null) {
                query = query + "&year=" + program.getYear();
            }
            String response = comms.getURLAsString(query);
            if (response.contains("Film not found")) {
                if (program.getYear() == null) {
                    logger.debug("Didn't find metadata for {}, giving up", program.getTitle());
                    return;
                }
                logger.debug("Didn't find metadata for {} {}, trying one up", program.getTitle(), program.getYear());
                query = "http://imdbapi.org/?q=" + encode(program.getTitle()) + "&year=" + (Integer.valueOf(program.getYear())+1) + "&yg=1";
                response = comms.getURLAsString(query);
                if (response.contains("Film not found")) {
                    logger.debug("Didn't find metadata for {} {}, trying one down", program.getTitle(), program.getYear());
                    query = "http://imdbapi.org/?q=" + encode(program.getTitle()) + "&year=" + (Integer.valueOf(program.getYear())-1) + "&yg=1";
                    response = comms.getURLAsString(query);
                    if (response.contains("Film not found")) {
                        logger.debug("Didn't find metadata for {} {}, one up or down, giving up", program.getTitle(), program.getYear());
                        return;
                    }
                }
            }
            DeanClatworthyMetadata metadata = mapper.readValue(response, DeanClatworthyMetadata.class);
            program.setImdbId(metadata.imdbid);
            program.setImdbRating(convertString(metadata.rating));
            program.setImdbSource(SOURCE);
        } catch (IOException e) {
            logger.error("Error parsing metadata: " + e.getMessage(), e);
            throw new RuntimeException("Error parsing metadata: " + e.getMessage(), e);
        }
    }
}
