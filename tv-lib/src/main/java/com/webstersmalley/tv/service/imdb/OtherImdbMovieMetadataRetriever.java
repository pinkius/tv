package com.webstersmalley.tv.service.imdb;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.webstersmalley.tv.comms.Comms;
import com.webstersmalley.tv.domain.Program;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;

import static com.webstersmalley.tv.service.imdb.ImdbUtils.convertString;
import static com.webstersmalley.tv.service.imdb.ImdbUtils.encode;
/**
 * Created by: Matthew Smalley
 * Date: 17/05/13
 */
@Service("omdbApiImdbDataRetriever")
public class OtherImdbMovieMetadataRetriever implements ImdbDataRetriever {
    private final static String SOURCE = "omdbapi.com";
    private Logger logger = LoggerFactory.getLogger(getClass());

    @Resource(name = "longtimeoutComms")
    private Comms comms;

    private ObjectMapper mapper = new ObjectMapper();

    public OtherImdbMovieMetadataRetriever() {
        mapper.configure(JsonGenerator.Feature.QUOTE_FIELD_NAMES, false);
        mapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
    }

    public MovieMetadata getMovieMetadata(Program program) {
        try {
            String query = "http://www.omdbapi.com/?t=" + encode(program.getTitle());
            if (program.getYear() != null) {
                query = query + "&y=" + program.getYear();
            }
            String response = comms.getURLAsString(query);
            OtherImdbMetadata metadata1 = mapper.readValue(response, OtherImdbMetadata.class);
            MovieMetadata metadata = new MovieMetadata();
            metadata.setImdb_id(metadata1.getImdbID());
            metadata.setRating(new BigDecimal(metadata1.getImdbRating().toString()));
            return metadata;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private String getResponseForQuery(String title, String year) {
        String query = "http://www.omdbapi.com/?t=" + encode(title);
        if (year != null) {
            query = query + "&y=" + year;
        }
        return comms.getURLAsString(query);
    }

    @Override
    public void addImdbData(Program program) {
        try {
            String response = getResponseForQuery(program.getTitle(), program.getYear());
            if (response == null || response.contains("Movie not found!")) {
                response = getResponseForQuery(program.getTitle(), ""+((Integer.valueOf(program.getYear())+1)));
                if (response == null || response.contains("Movie not found!")) {
                    response = getResponseForQuery(program.getTitle(), ""+((Integer.valueOf(program.getYear())-1)));
                }
            }
            if (response == null || response.contains("Movie not found!")) {
                logger.debug("Found no satisfaction, giving up");
                return;
            }
            OtherImdbMetadata metadata1 = mapper.readValue(response, OtherImdbMetadata.class);
            program.setImdbId(metadata1.getImdbID());
            program.setImdbRating(convertString(metadata1.getImdbRating()));
            program.setImdbSource(SOURCE);

        } catch (Exception e) {
            logger.error("Error getting data for movie: {} {}", program.getTitle(), e.getMessage());
        }
    }
}
