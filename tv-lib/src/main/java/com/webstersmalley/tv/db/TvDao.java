/*
 * Copyright 2013 Webster Smalley
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.webstersmalley.tv.db;

import com.webstersmalley.tv.domain.Channel;
import com.webstersmalley.tv.domain.Program;
import com.webstersmalley.tv.domain.Recording;
import com.webstersmalley.tv.domain.SearchQuery;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public interface TvDao {
    List<Channel> getChannels();

    void addChannel(Channel channel);

    Channel getChannel(String channelName);

    void addProgram(Program program);

    List<Program> getProgramsByTitle(String programTitle);

    List<Program> getProgramsBySearchQuery(SearchQuery searchQuery);

    Channel getChannelById(int id);

    List<Program> getProgramsByTitleAndChannel(String programTitle, String channel);

    List<Program> getProgramsByChannel(Channel channel);

    Program getProgramByDetails(Program program);

    void clearAllPrograms(Channel channel);

    void addRecording(Recording recording);

    List<Recording> getAllRecordings();

    void deleteAllRecordings();

    List<Map<String, Object>> getAllRecordingsAsMap();

    long getQueryTime();

    List<Program> getMovies(BigDecimal minimumRating);

    List<Program> getProgramSummaries();
}
