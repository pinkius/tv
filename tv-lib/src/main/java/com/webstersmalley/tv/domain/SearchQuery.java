/*
 * Copyright 2013 Webster Smalley
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.webstersmalley.tv.domain;

import org.apache.commons.lang3.builder.CompareToBuilder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

/**
 * Created by: Matthew Smalley
 * Date: 23/09/12
 */
public class SearchQuery implements Comparable<SearchQuery> {
    private int id = -1;

    private String seasonNumber;
    private String episodeNumber;
    private List<String> keywords;
    private String titleSearch;
    private boolean isMovie;

    public List<String> getKeywords() {
        return keywords;
    }

    public void setKeywords(List<String> keywords) {
        this.keywords = keywords;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSeasonNumber() {
        return seasonNumber;
    }

    public void setSeasonNumber(String seasonNumber) {
        this.seasonNumber = seasonNumber;
    }

    public String getEpisodeNumber() {
        return episodeNumber;
    }

    public void setEpisodeNumber(String episodeNumber) {
        this.episodeNumber = episodeNumber;
    }

    public String getTitleSearch() {
        return titleSearch;
    }

    public void setTitleSearch(String titleSearch) {
        this.titleSearch = titleSearch;
    }

    public boolean isMovie() {
        return isMovie;
    }

    public void setMovie(boolean isMovie) {
        this.isMovie = isMovie;
    }

    public boolean equals(Object object) {
        if (object == null) {
            return false;
        }
        if (object == this) {
            return true;
        }
        if (object.getClass() != getClass()) {
            return false;
        }
        SearchQuery rhs = (SearchQuery) object;
        return new EqualsBuilder()
                .append(seasonNumber, rhs.seasonNumber)
                .append(episodeNumber, rhs.episodeNumber)
                .append(keywords, rhs.keywords)
                .append(titleSearch, rhs.titleSearch)
                .append(isMovie, rhs.isMovie)
                .isEquals();
    }

    public int hashCode() {
        HashCodeBuilder hcb = new HashCodeBuilder(17, 31);

        return hcb
                .append(seasonNumber)
                .append(episodeNumber)
                .append(keywords)
                .append(titleSearch)
                .append(isMovie)
                .toHashCode();
    }

    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    public void validate() {
        if (seasonNumber == null && episodeNumber == null && (keywords == null || keywords.size() == 0) && titleSearch == null) {
            throw new RuntimeException("Invalid search query - no parameters set");
        }
    }

    @Override
    public int compareTo(SearchQuery o) {
        return new CompareToBuilder()
                .append(this.titleSearch, o.titleSearch)
                .append(this.seasonNumber, o.seasonNumber)
                .append(this.episodeNumber, o.episodeNumber)
                .append(this.keywords, o.keywords)
                .append(this.isMovie, o.isMovie)
                .toComparison();
    }

    public static SearchQuery fromString(String query) {
        SearchQuery searchQuery = new SearchQuery();
        if (query.startsWith("M:")) {
            searchQuery.setMovie(true);
            searchQuery.setTitleSearch(query.substring(2));
        } else if (query.contains(",")) {
            String[] bits = query.split(",");
            searchQuery.setTitleSearch(bits[0]);
            searchQuery.setSeasonNumber(bits[1]);
        } else {
            searchQuery.setTitleSearch(query);
        }
        return searchQuery;
    }
}
