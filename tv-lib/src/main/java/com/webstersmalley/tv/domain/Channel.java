/*
 * Copyright 2013 Webster Smalley
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.webstersmalley.tv.domain;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class Channel implements Comparable<Channel> {
    private int id;
    private String name;

    public final int getId() {
        return id;
    }

    public final void setId(int id) {
        this.id = id;
    }

    // Gets the name
    public final String getName() {
        return name;
    }

    public final void setName(String name) {
        this.name = name;
    }

    public boolean equals(Object object) {
        if (object == null) {
            return false;
        }
        if (object == this) {
            return true;
        }
        if (object.getClass() != getClass()) {
            return false;
        }
        Channel rhs = (Channel) object;
        return new EqualsBuilder()
                .append(id, rhs.id)
                .isEquals();
    }

    public int hashCode() {
        HashCodeBuilder hcb = new HashCodeBuilder(17, 31);

        return hcb
                .append(id)
                .toHashCode();
    }

    public String toString() {
        return ToStringBuilder.reflectionToString(this).toString();
    }

    @Override
    public int compareTo(Channel o) {
        if (o == null) {
            return -1;
        } else {
            return name.compareTo(o.name);
        }
    }
}
