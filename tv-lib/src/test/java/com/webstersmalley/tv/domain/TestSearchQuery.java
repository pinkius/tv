/*
 * Copyright 2013 Webster Smalley
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.webstersmalley.tv.domain;

import junit.framework.Assert;
import org.junit.Test;

/**
 * Created by: Matthew Smalley
 * Date: 11/02/13
 */
public class TestSearchQuery {
    @Test
    public void testBasicSearchQuery() {
        SearchQuery searchQuery = SearchQuery.fromString("TestSearch");
        Assert.assertEquals("TestSearch", searchQuery.getTitleSearch());
    }

    @Test
    public void testSearchQueryWithSeasonNumber() {
        SearchQuery searchQuery = SearchQuery.fromString("TestSearch,5");
        Assert.assertEquals("TestSearch", searchQuery.getTitleSearch());
        Assert.assertEquals("5", searchQuery.getSeasonNumber());
    }

}
