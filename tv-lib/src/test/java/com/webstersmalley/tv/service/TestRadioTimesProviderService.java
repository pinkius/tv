/*
 * Copyright 2013 Webster Smalley
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.webstersmalley.tv.service;

import com.webstersmalley.tv.comms.Comms;
import com.webstersmalley.tv.domain.Channel;
import com.webstersmalley.tv.domain.Program;
import org.apache.commons.io.IOUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.List;
import java.util.Set;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 * Created by: Matthew Smalley
 * Date: 09/04/12
 */
public class TestRadioTimesProviderService {
    private RadioTimesProviderService radioTimesProvider;

    @Mock
    private Comms comms;

    @Before
    public void setupMocks() {
        MockitoAnnotations.initMocks(this);
        try {
            when(comms.getURLAsString(matches(".*channels.dat"))).thenReturn(IOUtils.toString(getClass().getResourceAsStream("/dev/channels.dat")));
            when(comms.getURLAsString(matches(".*1.dat"))).thenReturn(IOUtils.toString(getClass().getResourceAsStream("/dev/1.dat")));
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage(), e);
        }
        radioTimesProvider = new RadioTimesProviderService();
        radioTimesProvider.setComms(comms);
    }

    @Test
    public void testGetChannels() {
        List<Channel> channels = radioTimesProvider.getChannels();
        assertEquals(4, channels.size());
    }

    @Test
    public void testGetProgramsForChannel() {
        List<Channel> channels = radioTimesProvider.getChannels();
        Set<Program> programs = null;
        for (Channel channel : channels) {
            if (channel.getId() == 1) {
                programs = radioTimesProvider.getProgramsForChannel(channel);
            }
        }
        assertEquals(3, programs.size());
        for (Program program : programs) {
            if ("ProgramTitle".equals(program.getTitle())) {
                assertEquals("ProgramTitle", program.getTitle());
                assertEquals("ProgramSubtitle", program.getSubtitle());
                assertEquals(false, program.isRepeat());
                assertEquals(false, program.isNewSeries());
                assertEquals("Description of the program", program.getDescription());
                DateTimeFormatter dtf = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm");
                DateTime expectedStart = dtf.parseDateTime("15/05/2013 20:00");
                assertEquals(expectedStart, program.getStartTime());
                assertEquals(60, program.getDuration());
                Channel channel = new Channel();
                channel.setId(1);
                channel.setName("Channel 1");
                assertEquals(channel, program.getChannel());
            }
        }

    }

    private void checkEpisodes(String input, String expectedEpisodeNumber, String expectedSeasonNumber) {
        Program program = new Program();
        String[] bits = new String[2];
        bits[1] = input;
        radioTimesProvider.parseEpisodeNumber(bits, program);
        assertEquals(program.getEpisodeNumber(), expectedEpisodeNumber);
        assertEquals(program.getSeasonNumber(), expectedSeasonNumber);
    }

    @Test
    public void testEpisodeParser() {
        checkEpisodes("", null, null);
        checkEpisodes("series 41", null, "41");
        checkEpisodes("4908", "4908", null);
        checkEpisodes("13, series 31", "13", "31");
        checkEpisodes("13/26, series 20", "13", "20");
    }


    @Test
    public void testParseRepeats() {
        Comms dummyComms = mock(Comms.class);
        when(dummyComms.getURLAsString(any(String.class))).thenReturn("Being Human~1/6, series 5~The Trinity~~Philip John~Alex*Kate Bracken|Tom*Michael Socha|Hal*Damien Molony|Mr Rook*Steven Robertson|Captain Hatch*Phil Davis|Crumb*Colin Hoult|Patsy*Claire Cage|Alistair Frith*Toby Whithouse|Lady Catherine*Victoria Ross|Martin*Wayne Cater|Emil*Jeremy Swift|Sophie*Non Haf|Hetty*Madeleine Harris~false~false~false~true~true~true~false~false~~~Drama~In the aftermath of the clash with the Old Ones, Alex begins to adjust to her new existence as a ghost. Hal struggles with withdrawal symptoms as he tries to kick the blood habit, but having been fired from the cafe, both he and Tom need to find new jobs. However, taking up positions at Barry's Grand Hotel brings them into contact with Captain Hatch, an unpleasant pensioner whose decrepit appearance belies an even darker secret. Supernatural comedy drama, starring Kate Bracken, Michael Socha and Damien Molony, with guest Phil Davis.~false~03/02/2013~22:00~23:00~60");

        RadioTimesProviderService provider = new RadioTimesProviderService();
        provider.setComms(dummyComms);
        Channel dummyChannel = new Channel();
        dummyChannel.setId(10);
        Set<Program> programs = provider.getProgramsForChannel(dummyChannel);
        assertEquals(1, programs.size());
        for (Program program : programs) {
            assertFalse(program.isRepeat());
            assertTrue(program.isNewSeries());
        }
    }

}
