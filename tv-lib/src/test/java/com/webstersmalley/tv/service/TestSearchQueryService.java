/*
 * Copyright 2013 Webster Smalley
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.webstersmalley.tv.service;

import com.webstersmalley.tv.AbstractSpringEnabledTests;
import com.webstersmalley.tv.db.SearchQueryDao;
import com.webstersmalley.tv.domain.SearchQuery;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Collections;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by: Matthew Smalley
 * Date: 14/10/12
 */
public class TestSearchQueryService extends AbstractSpringEnabledTests {
    private SearchQueryServiceImpl searchQueryService;

    @Mock
    private SearchQueryDao searchQueryDao;

    @Before
    public void setupMocks() {
        MockitoAnnotations.initMocks(this);
        when(searchQueryDao.getSearchQueries()).thenReturn(Collections.singletonList(new SearchQuery()));
        searchQueryService = new SearchQueryServiceImpl();
        searchQueryService.setSearchQueryDao(searchQueryDao);
    }

    @Test
    public void testGetQueries() {
        assertTrue(searchQueryService.getSearchQueries().size() == 1);
        verify(searchQueryDao).getSearchQueries();
    }

    @Test
    public void testSaveQuery() {
        SearchQuery sq = new SearchQuery();
        searchQueryService.saveSearchQuery(sq);
        verify(searchQueryDao).saveSearchQuery(sq);
    }
}
