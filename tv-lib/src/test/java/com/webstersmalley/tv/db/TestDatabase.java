/*
 * Copyright 2013 Webster Smalley
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.webstersmalley.tv.db;

import com.webstersmalley.tv.AbstractSpringEnabledTests;
import com.webstersmalley.tv.domain.Channel;
import com.webstersmalley.tv.domain.Program;
import com.webstersmalley.tv.domain.SearchQuery;
import com.webstersmalley.tv.service.ListingsRefresher;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;

public class TestDatabase extends AbstractSpringEnabledTests {
    @Resource(name = "tvDao")
    private TvDao tvDao;

    @Resource(name = "listingsRefresher")
    private ListingsRefresher listingsRefresher;

    @Resource(name = "tableDumper")
    private TableDumper tableDumper;

    @Before
    public void setupListings() {
        listingsRefresher.refreshListings();
    }

    @Test
    public void testSearchJustByTitle() {
        SearchQuery searchQuery = new SearchQuery();
        searchQuery.setTitleSearch("Drama");
        int count = tvDao.getProgramsBySearchQuery(searchQuery).size();
        assertTrue(count > 0);

        searchQuery.setKeywords(new ArrayList<String>());
        int count2 = tvDao.getProgramsBySearchQuery(searchQuery).size();
        assertTrue(count == count2);
    }

    @Test
    public void testSearch() {
        SearchQuery searchQuery = new SearchQuery();
        searchQuery.setTitleSearch("Title");
        List<Program> programs = tvDao.getProgramsBySearchQuery(searchQuery);
        tableDumper.dumpTable("select * from programs");
        assertEquals(3, programs.size());

        searchQuery.setSeasonNumber("8");
        programs = tvDao.getProgramsBySearchQuery(searchQuery);
        assertEquals(2, programs.size());

        searchQuery.setEpisodeNumber("8");
        programs = tvDao.getProgramsBySearchQuery(searchQuery);
        assertEquals(1, programs.size());
        for (Program program : programs) {
            assertTrue(8 <= Integer.parseInt(program.getEpisodeNumber()));
        }
    }


    @Test
    public void testGetChannels() {
        tvDao.getChannels().size();
    }

    @Test
    public void testAddChannel() {
        int originalChannelSize = tvDao.getChannels().size();

        Channel channel = new Channel();
        channel.setId(-50);
        channel.setName("Testing");
        tvDao.addChannel(channel);

        assertTrue(tvDao.getChannels().size() > originalChannelSize);
    }

    @Test
    public void testAddNewProgram() {
        Program program = new Program();
        program.setTitle("This is the program");
        program.setStartTime(new DateTime());
        program.setChannel(tvDao.getChannels().get(0));

        assertNull(tvDao.getProgramByDetails(program));
        tvDao.addProgram(program);
        assertNotNull(tvDao.getProgramByDetails(program));
    }

    @Ignore("Temporarily removed dupe check")
    @Test
    public void testAddDuplicateProgram() {
        DateTime startTime = new DateTime();
        Program program = new Program();
        program.setTitle("This is the program");
        program.setStartTime(startTime);
        program.setChannel(tvDao.getChannels().get(0));

        assertNull(tvDao.getProgramByDetails(program));
        tvDao.addProgram(program);
        assertNotNull(tvDao.getProgramByDetails(program));
        int id = tvDao.getProgramByDetails(program).getId();

        tvDao.addProgram(program);
        assertEquals(id, tvDao.getProgramByDetails(program).getId());

    }

    private boolean programExistsByKeywords(Program program, List<String> keywords) {
        SearchQuery searchQuery = new SearchQuery();
        searchQuery.setKeywords(keywords);
        List<Program> programs = tvDao.getProgramsBySearchQuery(searchQuery);
        boolean found = false;
        for (Program p : programs) {
            if (p.getTitle().equals(program.getTitle())) {
                found = true;
                break;
            }
        }
        return found;
    }

    @Ignore("Temporarily removed keywords")
    @Test
    public void testKeywords() {
        Program program = new Program();
        program.setTitle("A program with some keywords");
        program.setStartTime(new DateTime());
        program.setChannel(tvDao.getChannels().get(0));

        tableDumper.dumpTable("select * from keywords");

        assertFalse(programExistsByKeywords(program, Collections.singletonList("keywords")));
        tvDao.addProgram(program);

        tableDumper.dumpTable("select * from keywords");

        assertTrue(programExistsByKeywords(program, Collections.singletonList("keywords")));
    }


}
