/*
 * Copyright 2013 Webster Smalley
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.webstersmalley.tv.comms;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributeView;
import java.nio.file.attribute.FileTime;
import java.util.Calendar;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 * Created by: Matthew Smalley
 * Date: 26/01/13
 */
public class TestCachingComms {
    private CachingComms cachingComms;
    private String cacheFolder = "cache";
    @Mock
    private Comms dummyComms;

    @Before
    public void setup() {
        cleanupFolder(new File(cacheFolder));
        cachingComms = new CachingComms();
        cachingComms.setCacheFolder(cacheFolder);
        cachingComms.setValidationTimeout(1000);

        MockitoAnnotations.initMocks(this);
        when(dummyComms.getURLAsString(any(String.class))).thenReturn("Hello everybody");
        cachingComms.setTargetComms(dummyComms);
    }

    @After
    public void cleanup() {
        cleanupFolder(new File(cacheFolder));
    }

    @Test
    public void testCreateNullFolder() {
        try {
            cachingComms.setCacheFolder(null);
            cachingComms.createFolderIfNeeded();
            fail();
        } catch (RuntimeException e) {
            // Expect this to fail with an exception!
        }
    }

    @Test
    public void testCreateExistingFolder() {
        File f = new File(cacheFolder);
        f.mkdir();
        cachingComms.createFolderIfNeeded();
        f.delete();
    }

    @Test
    public void testCreateFolderThatExistsAsFile() {
        try {
            File f = new File(cacheFolder);
            f.createNewFile();
            try {
                cachingComms.createFolderIfNeeded();
                fail("Should have failed as directory exists as file");
            } catch (RuntimeException e) {
                f.delete();
            }
        } catch (IOException e) {
            e.printStackTrace();
            fail(e.getMessage());
        }
    }

    @Test
    public void testCreateFolderSuccess() {
        File f = new File(cacheFolder);
        assertFalse(f.exists());
        cachingComms.createFolderIfNeeded();
        assertTrue(f.exists() && f.isDirectory());
        f.delete();
    }

    private void cleanupFolder(File f) {
        try {
            FileUtils.deleteDirectory(f);
        } catch (IOException e) {
            e.printStackTrace();
            fail(e.getMessage());
        }
    }


    @Test
    public void testServingUncachedFile() {
        File f = new File(cacheFolder);
        assertFalse(f.exists());
        cachingComms.getURLAsString("http://www.server.com/path/to/resource.html");

        assertTrue(f.exists() && f.isDirectory() && f.listFiles().length == 1);
        verify(dummyComms, times(1)).getURLAsString("http://www.server.com/path/to/resource.html");

        for (File f2 : f.listFiles()) {
            f2.delete();
        }
        f.delete();
    }

    @Test
    public void testServingOldFileWithUpdates() {
        try {
            File f = new File(cacheFolder);
            assertFalse(f.exists());
            cachingComms.setValidationTimeout(60 * 30);
            // Cache file doesn't exist, so should call the underlying
            cachingComms.getURLAsString("http://www.server.com/path/to/resource.html");
            verify(dummyComms, times(1)).getURLAsString("http://www.server.com/path/to/resource.html");
            assertEquals(1, f.listFiles().length);

            Path p = FileSystems.getDefault().getPath(cacheFolder, f.listFiles()[0].getName());
            BasicFileAttributeView view = Files.getFileAttributeView(p, BasicFileAttributeView.class);
            FileTime time = FileTime.fromMillis(Calendar.getInstance().getTimeInMillis() - (1000 * 60 * 60));
            view.setTimes(time, time, time);

            // Cache file is too old, so should call the underlying
            cachingComms.getURLAsString("http://www.server.com/path/to/resource.html");
            verify(dummyComms, times(2)).getURLAsString("http://www.server.com/path/to/resource.html");

            // Cache file is new enough, so should serve up the cached content
            cachingComms.getURLAsString("http://www.server.com/path/to/resource.html");
            verify(dummyComms, times(2)).getURLAsString("http://www.server.com/path/to/resource.html");


        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }

    }

    @Test
    public void testServingCachedUnexpiredFile() {
        File f = new File(cacheFolder);
        assertFalse(f.exists());
        cachingComms.getURLAsString("http://www.server.com/path/to/resource.html");
        assertTrue(f.exists() && f.isDirectory() && f.listFiles().length == 1);
        cachingComms.getURLAsString("http://www.server.com/path/to/resource.html");

        verify(dummyComms, times(1)).getURLAsString("http://www.server.com/path/to/resource.html");

        for (File f2 : f.listFiles()) {
            f2.delete();
        }
        f.delete();
    }

    @Test
    public void testServingCachedExpiredFile() {
        File f = new File(cacheFolder);
        assertFalse(f.exists());
        cachingComms.setValidationTimeout(-1);
        cachingComms.getURLAsString("http://www.server.com/path/to/resource.html");
        assertTrue(f.exists() && f.isDirectory() && f.listFiles().length == 1);
        cachingComms.getURLAsString("http://www.server.com/path/to/resource.html");

        verify(dummyComms, times(2)).getURLAsString("http://www.server.com/path/to/resource.html");

        for (File f2 : f.listFiles()) {
            f2.delete();
        }
        f.delete();
    }

}
