/*
 * Copyright 2013 Webster Smalley
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.webstersmalley.tv.webapp.controller;

import com.webstersmalley.tv.domain.Channel;
import com.webstersmalley.tv.domain.Program;
import com.webstersmalley.tv.service.ListingsRefresher;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertTrue;

/**
 * Created by: Matthew Smalley
 * Date: 11/04/12
 */
public class TestTVGuideController extends AbstractSpringEnabledTests {
    @Resource(name = "tvGuideController")
    TVGuideController tvGuideController;

    @Resource(name = "listingsRefresher")
    private ListingsRefresher listingsRefresher;

    @Before
    public void setupListings() {
        listingsRefresher.refreshListings();
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testShowListings() {
        ModelAndView mav = tvGuideController.showListings(1);
        Channel channel = ((List<Channel>) mav.getModelMap().get("channels")).get(0);
        Map<Channel, Map<DateTime, List<Program>>> channelMappedPrograms = (Map<Channel, Map<DateTime, List<Program>>>) mav.getModelMap().get("channelMappedPrograms");

        Map<DateTime, List<Program>> dateMappedPrograms = channelMappedPrograms.get(channel);
        List<DateTime> dates = (List<DateTime>) mav.getModelMap().get("dates");
        for (DateTime date : dates) {
            List<Program> programList = dateMappedPrograms.get(date);
            ProgramComparator pc = new ProgramComparator();
            for (int i = 0; i < programList.size() - 1; i++) {
                assertTrue(pc.compare(programList.get(i), programList.get(i + 1)) <= 0);

            }
        }
    }

    @Test
    public void testSearchListings() {

    }

}
