/*
 * Copyright 2013 Webster Smalley
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.webstersmalley.tv.webapp.controller;

import com.webstersmalley.tv.domain.Channel;
import com.webstersmalley.tv.domain.Program;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.TreeSet;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by: Matthew Smalley
 * Date: 11/04/12
 */
public class TestProgramComparator {
    private Channel ch1;
    private Program ch1P1;
    private Program ch1P2;
    private Program ch1P3;

    private Channel ch2;
    private Program ch2P1;

    private ProgramComparator pc;

    @Before
    public void setup() {
        ch1 = new Channel();
        ch1.setId(1);

        ch2 = new Channel();
        ch2.setId(2);

        ch1P1 = new Program();
        ch1P1.setChannel(ch1);
        ch1P1.setStartTime(new DateTime());
        ch1P1.setTitle("Channel1 Program1");

        ch1P2 = new Program();
        ch1P2.setChannel(ch1);
        ch1P2.setStartTime(new DateTime().plus(10));
        ch1P2.setTitle("Channel1 Program2");

        ch1P3 = new Program();
        ch1P3.setChannel(ch1);
        ch1P3.setStartTime(ch1P1.getStartTime());
        ch1P3.setTitle("Channel1 Program3");

        pc = new ProgramComparator();

        ch2P1 = new Program();
        ch2P1.setChannel(ch2);
        ch2P1.setStartTime(ch1P1.getStartTime());
        ch2P1.setTitle("Channel2 Program1");

    }

    @Test
    public void testComparator() {
        assertTrue(pc.compare(ch1P1, ch1P2) < 0);
        assertTrue(pc.compare(ch1P1, ch1P3) != 0);
        assertTrue(pc.compare(ch1P1, ch2P1) < 0);
    }

    @Test
    public void testTreeSet() {
        List<Program> list = new ArrayList<Program>();
        list.add(ch2P1);
        list.add(ch1P3);
        list.add(ch1P2);
        list.add(ch1P1);

        TreeSet<Program> treeSet = new TreeSet<Program>(pc);
        treeSet.addAll(list);

        assertEquals(4, treeSet.size());

        Iterator<Program> it = treeSet.iterator();
        assertEquals(ch1P1, it.next());
        assertEquals(ch1P3, it.next());
        assertEquals(ch1P2, it.next());
        assertEquals(ch2P1, it.next());
    }
}
