/*
 * Copyright 2013 Webster Smalley
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.webstersmalley.tv.webapp.controller;

import com.webstersmalley.tv.db.SearchQueryDao;
import com.webstersmalley.tv.db.TvDao;
import com.webstersmalley.tv.domain.Channel;
import com.webstersmalley.tv.domain.Program;
import com.webstersmalley.tv.domain.SearchQuery;
import com.webstersmalley.tv.service.ListerService;
import com.webstersmalley.tv.service.ListingsRefresher;
import com.webstersmalley.tv.service.SearchQueryLoaderService;
import com.webstersmalley.tv.service.SearchQueryService;
import com.webstersmalley.tv.sky.SkyService;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Matthew Smalley
 */
@Controller("tvGuideController")
public class TVGuideController {
    protected final Logger logger = Logger.getLogger(getClass());

    @Resource(name = "searchQueryService")
    private SearchQueryService searchQueryService;

    @Resource(name = "listerService")
    private ListerService listerService;

    @Resource(name = "listingsRefresher")
    private ListingsRefresher listingsRefresher;

    @Resource(name = "skyService")
    private SkyService skyService;

    @Resource(name = "tvDao")
    private TvDao tvDao;
    @Resource(name = "searchQueryDao")
    private SearchQueryDao searchQueryDao;
    @Resource(name = "searchQueryLoaderService")
    private SearchQueryLoaderService searchQueryLoaderService;

    @RequestMapping("/listChannels")
    public ModelAndView listChannels() {

        ModelAndView mav = new ModelAndView("channels");

        List<Channel> channels = listerService.getChannels();

        System.out.println(channels.size());
        mav.addObject("channelList", channels);
        return mav;
    }

    @RequestMapping(value = "/showListings", method = RequestMethod.GET)
    public ModelAndView showListings(@RequestParam int channelId) {
        ModelAndView mav = new ModelAndView("listings");
        Channel channel = listerService.getChannelById(channelId);
        List<Program> programs = listerService.getProgramsByChannel(channel);
        Collections.sort(programs, new ProgramComparator());

        Map<Channel, Map<DateTime, List<Program>>> channelMappedPrograms = getChannelAndDateMappedPrograms(programs);

        mav.addObject("channelMappedPrograms", channelMappedPrograms);
        mav.addObject("dates", getSortedListOfDatesFromMap(channelMappedPrograms));
        mav.addObject("channels", getSortedListOfChannelsFromMap(channelMappedPrograms));

        return mav;
    }

    private Map<Channel, Map<DateTime, List<Program>>> getChannelAndDateMappedPrograms(List<Program> programs) {
        Map<Channel, Map<DateTime, List<Program>>> map = new HashMap<Channel, Map<DateTime, List<Program>>>();
        for (Program program : programs) {
            Channel channel = program.getChannel();
            if (!map.containsKey(channel)) {
                map.put(channel, new HashMap<DateTime, List<Program>>());
            }
            Map<DateTime, List<Program>> channelMap = map.get(channel);
            DateTime truncatedDate = program.getStartTime().withMillisOfDay(0);
            if (!channelMap.containsKey(program.getStartTime().withMillisOfDay(0))) {
                channelMap.put(truncatedDate, new ArrayList<Program>());
            }
            channelMap.get(truncatedDate).add(program);
        }

        return map;
    }

    private List<DateTime> getSortedListOfDatesFromMap(Map<Channel, Map<DateTime, List<Program>>> channelMappedPrograms) {
        List<DateTime> dates = new ArrayList<DateTime>();
        for (Map<DateTime, List<Program>> submap : channelMappedPrograms.values()) {
            dates.addAll(submap.keySet());
        }
        Collections.sort(dates);
        return dates;
    }

    private List<Channel> getSortedListOfChannelsFromMap(Map<Channel, Map<DateTime, List<Program>>> channelMappedPrograms) {
        List<Channel> output = new ArrayList<Channel>();
        output.addAll(channelMappedPrograms.keySet());
        Collections.sort(output);
        return output;
    }

    @RequestMapping(value = "/searchListings", method = RequestMethod.GET)
    public ModelAndView searchListings(@RequestParam("searchQuery") String searchQueryString) {
        SearchQuery searchQuery = SearchQuery.fromString(searchQueryString);
        ModelAndView mav = new ModelAndView("listings");
        List<Program> programs = listerService.getProgramsBySearchQuery(searchQuery);
        Collections.sort(programs, new ProgramComparator());
        Map<Channel, Map<DateTime, List<Program>>> channelMappedPrograms = getChannelAndDateMappedPrograms(programs);
        mav.addObject("channelMappedPrograms", channelMappedPrograms);

        mav.addObject("channelMappedPrograms", channelMappedPrograms);
        mav.addObject("dates", getSortedListOfDatesFromMap(channelMappedPrograms));
        mav.addObject("channels", getSortedListOfChannelsFromMap(channelMappedPrograms));
        return mav;
    }

    @RequestMapping(value = "/tv")
    public ModelAndView tvMainPage() {
        ModelAndView mav = new ModelAndView("mainpage");
        List<Channel> channels = listerService.getChannels();
        mav.addObject("channelList", channels);
        return mav;
    }

    @RequestMapping("/refreshListings")
    public String refreshListings() {
        listingsRefresher.refreshListings();
        return "redirect:tv.html";
    }

    @RequestMapping("/favourites")
    public ModelAndView favourites() {
        ModelAndView mav = new ModelAndView("favourites");

        mav.addObject("searchQueries", searchQueryService.getSearchQueries());

        return mav;
    }

    @RequestMapping("/addNewFavourite.html")
    public String addNewFavourite(@RequestParam("seasonNumber") String seasonNumber, @RequestParam("episodeNumber") String episodeNumber, @RequestParam("keywords") String keywords, @RequestParam("titleSearch") String titleSearch) {
        SearchQuery searchQuery = new SearchQuery();
        searchQuery.setEpisodeNumber(episodeNumber);
        searchQuery.setSeasonNumber(seasonNumber);
        if (keywords != null && !keywords.equals("")) {
            List<String> keywordsList = new ArrayList<String>();
            for (String keywordString : keywords.split(" ")) {
                keywordsList.add(keywordString.toLowerCase().trim());
            }
            searchQuery.setKeywords(keywordsList);
        }
        searchQuery.setTitleSearch(titleSearch);

        logger.info("Saving search query: " + searchQuery);
        searchQueryService.saveSearchQuery(searchQuery);
        return "redirect:favourites.html";
    }

    @RequestMapping("/refreshRecordings")
    public String refreshRecordings() {
        skyService.importRecordings();
        return "redirect:tv.html";
    }

    @RequestMapping("/loadSearchQueries")
    public String loadSearchQueries() {
        searchQueryLoaderService.loadSearchQueries();
        return "redirect:tv.html";
    }


    @RequestMapping("/sky.html")
    public ModelAndView getSkyRecordings() {
        ModelAndView mav = new ModelAndView("recordings");

        mav.addObject("recordings", tvDao.getAllRecordingsAsMap());

        return mav;
    }

    @RequestMapping("/searchAllQueries.html")
    public ModelAndView searchAllQueries() {
        ModelAndView mav = new ModelAndView("searchResults");
        List<SearchQuery> searchQueries = searchQueryDao.getSearchQueries();
        Collections.sort(searchQueries);
        Map<SearchQuery, List<Program>> map = new HashMap<SearchQuery, List<Program>>();

        for (SearchQuery searchQuery : searchQueries) {
            List<Program> matchedPrograms = listerService.getProgramsBySearchQuery(searchQuery);
            map.put(searchQuery, matchedPrograms);
        }
        Collections.sort(searchQueries);
        mav.addObject("results", map);
        mav.addObject("oneWeekInTheFuture", new DateTime().plusDays(7));
        mav.addObject("searchQueries", searchQueries);

        return mav;
    }
}
