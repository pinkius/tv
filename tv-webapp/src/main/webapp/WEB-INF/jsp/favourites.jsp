<%@ include file="/WEB-INF/jsp/include.jsp" %>
<html>
    <head>
        <title>TV</title>
        <link href="css/stylesheet.css" rel="stylesheet" type="text/css"/>
        <link href="css/ui-lightness/jquery-ui-1.8.21.custom.css" rel="stylesheet" type="text/css"/>
        <script src="js/jquery-1.7.2.min.js"></script>
        <script src="js/jquery-ui-1.8.21.custom.min.js"></script>
    </head>
    <body>
        <div id="container">
            <div id="header"><a href="<c:url value='/'/>">TV</a></div>
            <form method="POST" action="addNewFavourite.html">
                <table>
                    <tr>
                        <th>&nbsp;</th>
                        <th>Season</th>
                        <th>Episode</th>
                        <th>Keywords</th>
                        <th>Title</th>
                    </tr>
                    <c:forEach items="${searchQueries}" var="searchQuery">
                        <tr>
                            <td></td>
                            <td>${searchQuery.seasonNumber}</td>
                            <td>${searchQuery.episodeNumber}</td>
                            <td>${searchQuery.keywords}</td>
                            <td>${searchQuery.titleSearch}</td>
                        </tr>
                    </c:forEach>

                        <tr>
                            <td><input type="submit" value="Add new"/></td>
                            <td><input name="seasonNumber"/></td>
                            <td><input name="episodeNumber"/></td>
                            <td><input name="keywords"/></td>
                            <td><input name="titleSearch"/></td>
                        </tr>
                </table>
            </form>
    </body>
</html>
