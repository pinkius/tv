<%@ include file="/WEB-INF/jsp/include.jsp" %>
<html>
    <head>
        <title>TV</title>
        <link href="css/stylesheet.css" rel="stylesheet" type="text/css"/>
        <link href="css/ui-lightness/jquery-ui-1.8.21.custom.css" rel="stylesheet" type="text/css"/>
        <script src="js/jquery-1.7.2.min.js"></script>
        <script src="js/jquery-ui-1.8.21.custom.min.js"></script>
    </head>
    <body>
        <div id="container">
            <div id="header"><a href="<c:url value='/'/>">TV</a></div>
            <table class="standardTable">
                <thead>
                <tr>
                    <th>Time</th>
                    <th>Title</th>
                    <th>Channel</th>
                    <th>Status</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${recordings}" var="recording">
                <tr>
                    <td><fmt:formatDate pattern="yyyy-MMM-dd HH:mm" value="${recording.startTime}"/> - <fmt:formatDate pattern="HH:mm" value="${recording.endTime}"/></td>
                    <td>${recording.title}</td>
                    <td>${recording.channelName}</td>
                    <td>${recording.recStatusName}</td>
                </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </body>
</html>
