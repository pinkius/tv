<%@ include file="/WEB-INF/jsp/include.jsp" %>
<html>
    <head>
        <title>TV</title>
        <link href="css/stylesheet.css" rel="stylesheet" type="text/css"/>
        <link href="css/ui-lightness/jquery-ui-1.8.21.custom.css" rel="stylesheet" type="text/css"/>
        <script src="js/jquery-1.7.2.min.js"></script>
        <script src="js/jquery-ui-1.8.21.custom.min.js"></script>
    </head>
    <body>
        <div id="container">
            <div id="header"><a href="<c:url value='/'/>">TV</a></div>
            <table class="standardTable">
                <thead>
                    <tr>
                        <th>Time</th>
                        <th>Title</th>
                        <th>Channel</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                <c:forEach items="${results}" var="result">
                    <tr>
                        <td colspan="5">${result.key.titleSearch}</td>
                    </tr>
                <c:forEach items="${result.value}" var="program">
                    <tr>
                        <td colspan="5">${program}</td>
                    </tr>
                </c:forEach>
                </c:forEach>
                </tbody>
            </table>
            <table class="standardTable">
                <thead>
                    <tr>
                        <th>Time</th>
                        <th>Title</th>
                        <th>Channel</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </body>
</html>
