<%@ include file="/WEB-INF/jsp/include.jsp" %>
<html>
    <head>
        <title>TV</title>
        <link href="css/stylesheet.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div id="container">
            <div id="header">TV</div>
            <div id="body">
                <hr/>
                <form action="searchListings.html" method="GET">
                    Search listings <input type="text" name="searchQuery"/> <input type="submit"/>
                    <a href="sky.html">Show Recordings</a>
                    <a href="searchAllQueries.html">Show Saved Search Results</a>
                </form>

                Browse listings
                <div style="height:250px;overflow:auto;">
                    <c:forEach items="${channelList}" var="channel">
                        <a href="showListings.html?channelId=${channel.id}">${channel.name}</a>
                        <br />
                    </c:forEach>
                </div>
                <hr/>
                <div id="miscInfoDiv">
                    <a href="refreshListings.html">Refresh Listings</a>
                    <a href="refreshRecordings.html">Refresh Recordings</a>
                    <a href="loadSearchQueries.html">Load Queries</a>
                </div>
            </div>

            <div id="footer">
                <hr/>
                <div class="legal" align="center">
                    Software and site copyright (c) 2012 Webstersmalley. TV listings content is from Radiotimes XMLTV feed.
                </div>
            </div>
        </div>
    </body>
</html>